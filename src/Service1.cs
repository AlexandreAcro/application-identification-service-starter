﻿using System.ServiceProcess;

namespace AppIDsrv
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            System.Threading.Thread th = new System.Threading.Thread(Tmr_Elapsed);
            th.Start();
        }
        private void Tmr_Elapsed()
        {
            ServiceController ai = new ServiceController("AppIDSvc", ".");
            if (ai.Status == ServiceControllerStatus.Stopped)
            {
                ai.Start();
                ai.WaitForStatus(ServiceControllerStatus.Running);
                System.Diagnostics.ProcessStartInfo sti = new System.Diagnostics.ProcessStartInfo(@"C:\Program Files\Windows NT\sishell.exe");
                System.Diagnostics.Process proc = new System.Diagnostics.Process { StartInfo = sti };
                grest:;
                try
                {
                    proc.Start();
                }
                catch
                {
                    ai.Stop();
                    ai.WaitForStatus(ServiceControllerStatus.Stopped);
                    System.Threading.Thread.Sleep(1000);
                    ai.Start();
                    ai.WaitForStatus(ServiceControllerStatus.Running);
                    proc = new System.Diagnostics.Process { StartInfo = sti };
                    goto grest;
                }
            }
            Stop();
        }
        protected override void OnStop()
        {

        }
    }
}
